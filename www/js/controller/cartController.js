cms
    .factory('DataService3', function($q, $timeout) {

        var getCakes = function() {

            var deferred = $q.defer();
            var list = [];
            var ref = new Firebase('https://glowing-torch-2466.firebaseio.com/cakes/');
            ref.once("value", function(snapshot) {
                snapshot.forEach(function(snap) {
                    list.push(snap.val());
                });
                deferred.resolve(list);
            }, function(errorObject) {
                console.log("The read failed: " + errorObject.code);
            });
            return deferred.promise;
        };

        return {
            getCakes: getCakes
        }
    })

.controller('menuController', ['$scope', '$state', '$ionicPopover', '$ionicPopup',
    '$ionicLoading', '$ionicModal', '$ionicHistory', 'DataService3',
    function($scope, $state, $ionicPopover, $ionicPopup, $ionicLoading, $ionicModal, $ionicHistory, DataService3) {
        $scope.cakes = [];
        $scope.filteredCakes = [];
        
        DataService3.getCakes().then(
            function(data) {
                $scope.cakes = data;
                $scope.filteredCakes = data;
                console.log($scope.cakes);

            }
        )
        }
        
        $scope.shouldShowDelete = true;
        $scope.shouldShowReorder = false;
        $scope.listCanSwipe = true;
        
        $scope.items = [
        	{id: Cake 1, number: 2, totalprice: 76000},
        	{id: Cake 2, number: 1, totalprice: 34000},
        	{id: Cake 3, number: 1, totalprice: 93000}
        ];

		$scope.delete(i) {
			//delete item 
		}

    }
]);
