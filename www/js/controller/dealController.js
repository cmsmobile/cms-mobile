cms
.factory('DataService1', function($q, $timeout) {

    var getDeals = function() {

        var deferred = $q.defer();
		var list =[];
		var ref = new Firebase('https://glowing-torch-2466.firebaseio.com/deals/');
		ref.once("value", function(snapshot) {
		  snapshot.forEach(function(snap){
			 list.push(snap.val()); 
		  });
		  deferred.resolve(list);
		}, function (errorObject) {
		  console.log("The read failed: " + errorObject.code);
		});
        return deferred.promise;
    };

    return {
        getDeals : getDeals
    }
})
.controller('dealController', ['$scope','$state','$ionicPopover','$ionicPopup',
	'$ionicLoading','$ionicModal','$ionicHistory', 'DataService1',
	function ($scope,$state,$ionicPopover,$ionicPopup,$ionicLoading,$ionicModal,$ionicHistory, DataService1) {
		$scope.items = [];
		DataService1.getDeals().then(
			function(deals) {
				$scope.items = deals;
			}
		)
		$scope.dealsDetail = function() {
            //go to deal detail
        }
        
        $scope.filterThisWeek = function() {
            $('#tab-all').removeClass('active');
            $('#tab-week').addClass('active');
            $('#tab-today').removeClass('active');
            $('#tab-hot').removeClass('active');

            //need to be updated for filter This Week
        }
        $scope.filterAll = function() {
            $('#tab-all').addClass('active');
            $('#tab-week').removeClass('active');
            $('#tab-today').removeClass('active');
            $('#tab-hot').removeClass('active');
			//need to be updated for filter All
        }
        $scope.filterToday = function() {
            $('#tab-all').removeClass('active');
            $('#tab-week').removeClass('active');
            $('#tab-today').addClass('active');
            $('#tab-hot').removeClass('active');
            //need to be updated for filter Today
        }
        $scope.filterHot = function() {
            $('#tab-all').removeClass('active');
            $('#tab-week').removeClass('active');
            $('#tab-today').removeClass('active');
            $('#tab-hot').addClass('active');
            //need to be updated for filter Hot
            
        }
}]);
